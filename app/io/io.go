package io

import "fmt"

func GetUserInputs() (string, string, string, uint8) {
	var firstName string
	var lastName string
	var email string
	var numberOfTickets uint8

	fmt.Printf("Enter your firstname: ")
	fmt.Scan(&firstName)

	fmt.Printf("Enter your lastname: ")
	fmt.Scan(&lastName)

	fmt.Printf("Enter your email: ")
	fmt.Scan(&email)

	fmt.Printf("Enter number of tickets: ")
	fmt.Scan(&numberOfTickets)

	return firstName, lastName, email, numberOfTickets
}