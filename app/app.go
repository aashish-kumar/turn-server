package app

import (
	"fmt"
	"turn-server/app/common"
	"turn-server/app/io"
	"turn-server/app/util"
	"turn-server/app/routine"
)

func App() {
	var remainingTickets uint8 = common.ConferenceTickets
	var bookings = make([]common.Booking, 0)

	for {
		util.GreetUsers(remainingTickets)

		firstName, lastName, email, numberOfTickets := io.GetUserInputs()

		isValidName, isValidEmail, isValidTicketCount :=
			util.ValidateUserInputs(firstName,
				lastName,
				email,
				numberOfTickets,
				remainingTickets)

		userInputsAreValid := isValidName && isValidEmail && isValidTicketCount

		if userInputsAreValid {
			remainingTickets -= numberOfTickets
			fmt.Printf("Thank you %v %v for booking %v tickets, you'll get"+
				" a confirmation at %v\n", firstName, lastName, numberOfTickets,
				email)

			fmt.Println("Remaining tickets are", remainingTickets)

			booking := common.Booking{
				FirstName: firstName,
				LastName: lastName,
				Email: email,
				NumberOfTickets: numberOfTickets,
			};
			bookings = append(bookings, booking)
			firstNames := util.GetUpdatedFirstNames(bookings)

			fmt.Printf("We have %v bookings till now: %v\n", len(bookings),
				firstNames)

			routine.WaitingGroup.Add(1)
			go util.SendConfirmationMail(firstName, email)

			noRemainingTickets := remainingTickets == 0
			if noRemainingTickets {
				fmt.Println("All tickets booked out, come back next year")
				break
			}
		} else {
			util.HandleInvalidInputs(
				isValidName,
				isValidEmail,
				isValidTicketCount,
				numberOfTickets,
				remainingTickets)
		}
	}

	routine.WaitingGroup.Wait()
}