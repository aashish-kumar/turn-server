package common

type Booking struct {
	FirstName string
	LastName string
	Email string
	NumberOfTickets uint8
}

const ConferenceName = "Go Conference"
const ConferenceTickets uint8 = 50
