package util

import (
	"fmt"
	"strings"
)

func ValidateUserInputs(firstName string, lastName string, email string,
	numberOfTickets uint8, remainingTickets uint8) (bool, bool, bool) {
	isValidName := len(firstName) >= 2 && len(lastName) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidTicketCount := numberOfTickets > 0 &&
		numberOfTickets <= remainingTickets

	return isValidName, isValidEmail, isValidTicketCount
}


func HandleInvalidInputs(isValidName bool, isValidEmail bool,
	isValidTicketCount bool, numberOfTickets uint8, remainingTickets uint8) {
	if !isValidName {
		fmt.Println("Firstname and lastname should have at least 2" +
			" characters")
	}
	if !isValidEmail {
		fmt.Println("Entered email is not correct")
	}
	if !isValidTicketCount {
		fmt.Printf("Cannot book %v tickets, only %v tickets are available"+
			" at the moment\n", numberOfTickets, remainingTickets)
	}
	fmt.Println("Your input is invalid, please try again!")
}
