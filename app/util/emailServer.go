package util

import (
	"fmt"
	"time"
	"turn-server/app/routine"
)

func SendConfirmationMail(firstName string, email string) {
	time.Sleep(5 * time.Second)
	fmt.Printf("##### Email is sent to %v at %v! #####\n", firstName, email)
	routine.WaitingGroup.Done()
}