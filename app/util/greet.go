package util

import (
	"fmt"
	"turn-server/app/common"
)

func GreetUsers(remainingTickets uint8) {
	fmt.Printf("Welcome to the %v booking application\n", common.ConferenceName)

	fmt.Println("We have these many tickets", common.ConferenceTickets,
		" and these are remaining ones", remainingTickets)
}