package util

import (
	"strings"
	"turn-server/app/common"
)

func GetUpdatedFirstNames(bookings []common.Booking) []string {
	firstNames := []string{}
	for _, booking := range bookings {
		name := strings.Fields(booking.FirstName)
		firstNames = append(firstNames, name[0])
	}
	return firstNames
}